Autonomous Arduino Car

This is a self driving car that gets activated when a specific amount of light hits on it. This car is able to avoid obstacles. It is consisted of these items:
1) Arduino Uno
2) L298N Motor Driver
3) Ultrasonic sensor
4) 2 DC Motors
5) Power Supply 9V
6) 1 Breadboard
7) 1 LED
8) 1 Photoresistor
9) 2 Resistors (220Ω and 10kΩ)
10) 1 Servo Motor

A demonstration video:

![](Videos/Autonomous_Arduino_Car.mp4.mp4)
